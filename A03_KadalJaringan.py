#!/usr/bin/python3
import socket, struct
from typing import Tuple

# IP initialization
BUFFER_SIZE = 2048
SERVER_IP = ""
SERVER_PORT = 5910

ASDOS_IP = "34.101.92.60"
ASDOS_PORT = 5353
ASDOS_ADDR = (ASDOS_IP,ASDOS_PORT)

def head_n_quest(req_msg: bytearray) -> list:
    ms = struct.unpack("!HHHHHH", req_msg[:12]) # Message Split
    ls = [] # List Split. Order: ID, QR, OPCODE, AA, TC, RD, RA, Z, AD, CD, RCODE, 
            # QDCOUNT, ANCOUNT, NSCOUNT, ARCOUNT, QNAME, QTYPE, QCLASS, cur
            # I'll explain what "cur" is later.

    # Part I: ID
    ls.append(int(ms[0]))

    # Part II: QR to RCODE. Let's call them "flag". It's a pretty cute name.
    # Let's just convert the whole hex into string to reduce our mental torture.
    flag = str(bin(ms[1])[2:].zfill(16))
    slic = [1, 4, 1, 1 ,1 ,1 ,1 ,1, 1, 4]
    s = 0
    for i in slic:
        e = s + i
        val = flag[s:e]
        s = e
        ls.append(int(val,2))

    # Part III: The Count
    for j in ms[2:6]:
        ls.append(int(j))

    # Part IV: QNAME 
    # The "cur" will be the current index position
    # on bytearray starting from the index 12.
    cur = 12
    qname = ""
    while req_msg[cur] != 0:
        if cur != 12:
            qname += "."
        part_len = int(req_msg[cur])
        for l in range(0, part_len):
           qname += chr(int(req_msg[cur+l+1])) 
        cur += part_len + 1
    ls.append(qname)

    # Part IV: The Final Qs and the end
    qm = struct.unpack("!HH", req_msg[cur+1:cur+5])
    ls.append(int(qm[0])) # QTYPE
    ls.append(int(qm[1])) # QCLASS
    ls.append(cur+5) # cur
    return ls

def request_parser(request_message_raw: bytearray, source_address: Tuple[str, int]) -> str:
    msg = head_n_quest(request_message_raw)
    out_str = f"""=========================================================================
[Request from ({source_address[0]}, {source_address[1]})]
-------------------------------------------------------------------------
HEADERS
Request ID: {msg[0]}
QR: {msg[1]} | OPCODE: {msg[2]} | AA: {msg[3]} | TC: {msg[4]} | RD: {msg[5]} | RA: {msg[6]} | AD: {msg[8]} | CD: {msg[9]} | RCODE: {msg[10]}
Question Count: {msg[11]} | Answer Count: {msg[12]} | Authority Count: {msg[13]} | Additional Count: {msg[14]}
-------------------------------------------------------------------------
QUESTION
Domain Name: {msg[15]} | QTYPE: {msg[16]} | QCLASS: {msg[17]}
-------------------------------------------------------------------------"""
    return out_str

def response_parser(response_message_raw: bytearray) -> str:
    msg = head_n_quest(response_message_raw)
    cur = msg[18] # msg order after head_n_quest:
                  # TYPE, CLASS, TTL, RDLENGTH, RDATA

    # Part I: Main Answer Part
    # We will omit the "NAME" type.
    ms = struct.unpack("!HHHIH", response_message_raw[cur:cur+12])
    for i in ms[1:5]:
        msg.append(int(i))

    # Part II: RDATA
    cur += 12
    rd = msg[22]
    rdata = ""
    start = cur
    for l in range(0, rd):
        if cur != start:
            rdata += "."
        rdata += str(int(response_message_raw[cur+l])) 
        cur += 1
    msg.append(rdata)

    out_str = f"""[Response from DNS Server]
-------------------------------------------------------------------------
HEADERS
Request ID: {msg[0]}
QR: {msg[1]} | OPCODE: {msg[2]} | AA: {msg[3]} | TC: {msg[4]} | RD: {msg[5]} | RA: {msg[6]} | AD: {msg[8]} | CD: {msg[9]} | RCODE: {msg[10]}
Question Count: {msg[11]} | Answer Count: {msg[12]} | Authority Count: {msg[13]} | Additional Count: {msg[14]}
-------------------------------------------------------------------------
QUESTION
Domain Name: {msg[15]} | QTYPE: {msg[16]} | QCLASS: {msg[17]}
-------------------------------------------------------------------------
ANSWER
TYPE: {msg[19]} | CLASS: {msg[20]} | TTL: {msg[21]} | RDLENGTH: {msg[22]}
IP Address: {msg[23]}
=========================================================================="""
    return out_str

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sc:
        sc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sc.bind((SERVER_IP, SERVER_PORT))

        while True:
            # Get the request from the client and parse it
            SOURCE_MESG, SOURCE_ADDR = sc.recvfrom(BUFFER_SIZE)
            print(request_parser(SOURCE_MESG, SOURCE_ADDR))
            
            # Send our message to the asdos server, get the response and parse it
            sc.sendto(SOURCE_MESG, ASDOS_ADDR)
            ASDOS_MESG, _ = sc.recvfrom(BUFFER_SIZE)
            print(response_parser(ASDOS_MESG))

            # Send the message back to our source
            sc.sendto(ASDOS_MESG, SOURCE_ADDR)

# DO NOT ERASE THIS PART! OR I'LL VISIT YOUR HOME AND INSTALL GENTOO TO ALL OF YOUR PC BY FORCE.
if __name__ == "__main__":
    main() 
